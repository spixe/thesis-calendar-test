# thesis-calendar-test


## Установка
```
npm install
```

### Запуск и горячая перезагрузка
```
npm run serve
```

### Сборка проекта
```
npm run build
```

### Запуск линтера
```
npm run lint
```

![image-1](https://gitlab.com/spixe/thesis-calendar-test/-/raw/master/screenshots/screen1.png)
![image-2](https://gitlab.com/spixe/thesis-calendar-test/-/raw/master/screenshots/screen2.png)
![image-3](https://gitlab.com/spixe/thesis-calendar-test/-/raw/master/screenshots/screen3.png)

