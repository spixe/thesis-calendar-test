/**
 * @typedef CalendarEvent
 * @property {Number} id
 * @property {String} name название
 * @property {Date} date дата
 * @property {'green' | 'red' | 'orange'} type тип
 */

/**
 * @type {Array<CalendarEvent>}
 */
 export const calendarEvents = [
  {
    id: 1,
    name: 'Некоторое событие 1',
    date: new Date(2021, 8, 30, 12, 10),
    type: 'green',
  },
  {
    id: 2,
    name: 'Случится что-то',
    date: new Date(2021, 9, 30, 10, 10),
    type: 'green',
  },
  {
    id: 3,
    name: 'Ничего не случится',
    date: new Date(2021, 10, 30, 10, 15),
    type: 'red',
  },
  {
    id: 4,
    name: 'Ничего не случится',
    date: new Date(2021, 10, 30, 10, 20),
    type: 'orange',
  },
  {
    id: 5,
    name: 'Ничего не случится',
    date: new Date(2021, 10, 30, 10, 30),
    type: 'green',
  },
  {
    id: 6,
    name: 'Ничего не случится',
    date: new Date(2021, 10, 30, 10, 40),
    type: 'green',
  },
  {
    id: 7,
    name: 'Новый год к нам мчится',
    date: new Date(2021, 11, 31, 22, 55),
    type: 'orange',
  },
  {
    id: 8,
    name: 'Новый год к нам мчится',
    date: new Date(2021, 11, 31, 23, 55),
    type: 'red',
  },
];
